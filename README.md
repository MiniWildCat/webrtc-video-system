<div align="center"> 
    <h1> 会议通讯室平台 </h1>
    <h5> 基于websocket的实时通信平台</h5>
    <img src="https://img.shields.io/badge/vue-3.0.0+-green.svg"/>
    <img src="https://img.shields.io/badge/element—plus-1.1.0+-green.svg"/> 
    <img src="https://img.shields.io/badge/express-4.18.1+-green.svg"/>
    <img src="https://img.shields.io/badge/socket.io-2.0.3+-green.svg"/>   
    <img src="https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/主页.jpg"/>   
    <img src="https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话-vue版本.jpg"/>    
</div>








### 一、项目介绍

#### 1.项目简介

​		基于WebRTC开发的视频系统，支持一对一点播，多对多视频通话，音视频录制等功能，采用vue + nodejs搭建的系统，可扩展为多平台应用。 最基本的操作，实现了更加优秀的功能和非凡的业务！


#### 2.项目优点

基于websoket的实时通话，以及音视频录制。

 

#### 3.博客文章

**webrtc学习：https://blog.csdn.net/qq_50792097/article/details/126552203**

**vue框架学习：https://blog.csdn.net/qq_50792097/article/details/126526411**



#### 4.项目截图 

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/主页.jpg "屏幕截图.png") | ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/照片拍摄.jpg "屏幕截图.png") |
| ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/音视频录制.jpg "屏幕截图.png") | ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话.jpg "屏幕截图.png") |
| ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话-加入.jpg "屏幕截图.png") | ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话-通话中.jpg "屏幕截图.png") |
| ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话-vue版本.jpg "屏幕截图.png") | ![输入图片说明](https://gitee.com/MiniWildCat/webrtc-video-system/raw/master/%E9%A1%B9%E7%9B%AE%E5%B1%95%E7%A4%BA%E5%9B%BE%E7%89%87/一对一通话-全栈版本.jpg "屏幕截图.png") |



#### 5.项目说明 

**1.turn服务器的问题**

经过测试，在局域网下，不需要turn服务器进行转发，可以直接通过p2p进行点对点连接



**2.信令服务器**

信令服务器是为了连接两个端点，进行媒体协商， 收集候选者，获取对方流的一个过程。



**3.正在运行的项目**

https://tadmin.fastcat.top/        前端vue项目

https://www.fastcat.top/         express后端项目(信令服务器)

经过测试，turn服务器穿透不了，无效，所以只有在同网络下（局域网），才能进行一对一的视频通话。   正常情况下，2022年11月之前有效，因为租的阿里云服务器快过期了！！！！



**4.现在开发的进度**

目前已经开发：一对一通话，音频录制，图片拍摄， 

未开发：视频会议，多人聊天室

ps—》也许后面工作也没机会开发了！哈哈，加油吧！各位，也许，上面的博客文章写的内容会帮助到你。

如果这个项目对你有启发的话，请star 一下，满足一下对 star的追求。




### 二、项目安装

#### 1.开始使用

使用git拉取当前项目

```

git clone https://gitee.com/MiniWildCat/webrtc-video-system.git

```

使用npm对vue前端页面和express后端系统 安装包

```

// 因为express采用的是nodemon实时刷新模块，所以需要首先全局安装nodemon
npm install nodemon -g

// 安装vue前端项目模块、express后端系统
yarn install 或者 npm  install 

```

> **注意事项：**
>
> 1. 因为vue前端项目中使用的是vue-socket.io模块的限制，只支持vue2模块，所以需要修改安装后的vue-socket.io的模块
> 2. 安装完模块后，找到node_modules/vue-socket.io/dist/vue-socketio.js文件，然后打开文件。
> 3. 再找到  “t.prototype.socket=this.io,t.prototype.socket=this.io,t.prototype.vueSocketIo=this“ 
> 4. 替换为 ”t.config.globalProperties.socket=this.io,t.config.globalProperties.socket=this.io,t.config.globalProperties.vueSocketIo = this“
> 5. 然后再启动 vue前端项目

运行项目

```

Vue前端页面 
yarn serve 或者 npm run serve

express后端系统（信令服务器）， 端口3001
yarn app 或者 npm run serve

express后端系统（turn中继服务器），端口3478 
（测试后，感觉这个turn服务器没有穿透功能，各位再找找其它的中继服务器吧！！！）
yarn server 或者 npm run server

```

项目开启后，可以通过以下url访问

```

http://localhost:8080/       Vue前端页面 

http://localhost:3001/       express后端系统 

```



#### 2.使用说明

1. 项目的架构可以下载使用
3. 经过本项目二次开发的项目，本项目不承担任何法律责任！！！



#### 3.开源许可 

会议通讯室平台采用 [Apache 2.0](http://www.apache.org/licenses/) 开源许可证。

​                        
