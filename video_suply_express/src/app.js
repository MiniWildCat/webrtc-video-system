const http = require("http")
const express = require('express')
const socketIo = require("socket.io")

// 1.创建express实例
const app = express()
// 2.创建http服务
const serve = http.createServer(app)
// 3.socketio和Http进行绑定
const io = socketIo(serve);
// 4.配置静态资源目录，（客户端页面）
app.use(express.static(__dirname + '/../public'))
// 5.全局变量设置
// （1.用户数量
const  USERCOUNT = 2
let config = {}
let x = 0

// 6.socketIO监听连接
io.sockets.on('connection',(socket)=>{
    console.log("新用户已经加入了",socket.id)
    // （1.监听进入房间
    socket.on('join',(room,secret)=>{
        console.log("数据是",room,secret)
        // 如果超过50个，就清除原有的链接
        let len = Object.keys(config).length;
        if(len > 50){
            config = {}
        }

        // 保存房间和密钥
        if(!config[room]){
            console.log("已经设置密码1",config,secret,len,x)
            config[room] = secret
            console.log("已经设置密码2",config,secret)
        }else{
            // 验证密码
            console.log("认证密码",config,secret)
            if(secret != config[room]){
                socket.emit("failed",'房间密码错误！无法加入房间')
                return
            }
        }
        socket.join(room);
        console.log("已经加入房间",room,secret)
        var myRoom =  io.sockets.adapter.rooms[room];
        var users = myRoom ? Object.keys(myRoom.sockets).length : 0;
        x = users
        console.log("当前房间的人数是",users)
        if(users<=USERCOUNT){
            socket.emit("joined",room,socket.id)
            if(users > 1){
                socket.to(room).emit("otherjoin",room,socket.id)
            }
        }else{
            socket.leave(room)
            socket.emit("full",room,socket.id)
        }
        // // logger.log("当前房间的人数是"+users)
        // // socket.emit('joined',room,socket.id)      // 给本次连接的指定的客户端发送消息
        // io.in(room).emit('joined',room,socket.id) // 给当前房间内所有人消息（io是最优秀的，可以管理所有用户）
        // // socket.to(room).emit('joined',room,socket.id)  // 除自己以外的当前房间内的发送信息
        // // socket.broadcast.emit('joined',room,socket.id) // 除自己的所有房间发送信息
    })
    // （2.监听离开房间
    socket.on('leave',(room)=>{
        var myRoom =  io.sockets.adapter.rooms[room];
        var users = myRoom ? Object.keys(myRoom.sockets).length : 0;
        // users - 1
        console.log("当前房间的人数是"+users-1)
        socket.leave(room)

        socket.to(room).emit('bye',room,socket.id)
        socket.emit('leaved',room,socket.id)

        // // // logger.log("当前房间的人数是"+users)
        // // // socket.emit('leaved',room,socket.id)      // 给本次连接的指定的客户端发送消息
        // io.in(room).emit('leaved',room,socket.id) // 给当前房间内所有人消息（io是最优秀的，可以管理所有用户）
        // // // socket.to(room).emit('leaved',room,socket.id)  // 除自己以外的当前房间内的发送信息
        // // // socket.broadcast.emit('leaved',room,socket.id) // 除自己的所有房间发送信息

    })
    // （3.监听发送消息(转发信息)
    socket.on('message',(room,data)=>{
        console.log(`当前房间${room}发送信息：${data}`)

        socket.to(room).emit('message',room,data)

        // // logger.log("当前房间的人数是"+users)
        // // socket.emit('message',room,socket.id)      // 给本次连接的指定的客户端发送消息
        // io.in(room).emit('message',room,socket.id,data) // 给当前房间内所有人消息（io是最优秀的，可以管理所有用户）
        // // socket.to(room).emit('message',room,socket.id)  // 除自己以外的当前房间内的发送信息
        // // socket.broadcast.emit('message',room,socket.id) // 除自己的所有房间发送信息
    })
    // (4.清除当前通话
    socket.on("over",(room)=>{
        socket.leave(room)

        socket.to(room).emit('bye',room,socket.id)
        socket.emit('leaved',room,socket.id)
        // 清除配置
        console.log("即将清除链接")
        Object.keys(config).forEach(item=>{
            if(item == null || item == room){
                delete config[item]
                console.log("已经清除连接",Object.keys(config).length)
            }
        })
    })
    // (5.暂时离开
    socket.on("pause",(room)=>{
        socket.leave(room)
        console.log("暂时离开")
    })

})

serve.listen(3001,function(){
    console.log("项目已启动 http://localhost:3001")
})