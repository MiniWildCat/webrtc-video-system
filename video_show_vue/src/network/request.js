import axios from 'axios'
import {api_domain} from 'config'

export const request = (config)=>{
    // 创建一个axios实例
    const instance = axios.create({
        baseURL:api_domain,
        timeout:20000
    })

    // 设置拦截器
    instance.interceptors.request.use((config)=>{
        return config
    },err=>{
        console.error('请求错误！',err)
    })
    instance.interceptors.response.use((res)=>{
        return res.data ? res.data : res
    },err=>{
        console.error('响应错误！',err)
    })

    // 返回执行结果
    return instance(config)
}