import { createRouter, createWebHistory } from 'vue-router'




// 注意：不管是react，还是vue， 懒加载都是使用相对路径，不建议使用简写

const Home = ()=>import('../views/Home.vue')
const About = ()=>import('../views/About.vue')
const SingleCommunicate = ()=>import('../views/single/communicate.vue')
const MultipleMeeting = ()=>import('../views/multiple/meeting.vue')
const MultipleChat = ()=>import('../views/multiple/chat.vue')
const ToolsRecord = ()=>import('../views/tools/record.vue')
const ToolsCapture = ()=>import('../views/tools/capture.vue')


const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta:{ title: '会议通讯室__主界面'}
  },

  // 五大功能
  {
    path: '/single',
    name: 'Single',
    children:[
      {
        path:'communicate',
        name: 'SingleCommunicate',
        component: SingleCommunicate,
        meta:{ title: '会议通讯室__一对一视频通话'}
      }
    ]
  },

  {
    path: '/multiple',
    name: 'Multiple',
    children:[
      {
        path:'meeting',
        name: 'MultipleMeeting',
        component: MultipleMeeting,
        meta:{ title: '会议通讯室__视频会议'}
      },
      {
        path:'chat',
        name: 'MultipleChat',
        component: MultipleChat,
        meta:{ title: '会议通讯室__多人聊天室'}
      },
    ]
  },
  {
    path: '/tools',
    name: 'Tools',
    children:[
      {
        path:'record',
        name: 'ToolsRecord',
        component: ToolsRecord,
        meta:{ title: '会议通讯室__音频录制'}
      },
      {
        path:'capture',
        name: 'ToolsCapture',
        component: ToolsCapture,
        meta:{ title: '会议通讯室__图片拍摄'}
      },
    ]
  },


  {
    path: '/about',
    name: 'About',
    component: About
  },


  {
    // vue2 使用 * ， 则vue 3 需要正则匹配
    path:"/:pathMatch(.*)",
    name:"ErrorPage",
    redirect:{name:'Home'}
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


// 全局的前置勾子
router.beforeEach(async (to,from, next)=>{
  // 设置页面标题
  document.title = to.meta.title;
  next()
})


export default router
