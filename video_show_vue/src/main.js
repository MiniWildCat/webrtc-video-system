import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import {api_domain} from  'config';
// node_modules/vue-socketio/dist/vue-socketio.js
// 原来 t.prototype.$socket=this.io,t.prototype.$vueSocketIo=this
// 新内容 t.config.globalProperties.$socket=this.io,t.config.globalProperties.$vueSocketIo = this
import VueSocketIO from 'vue-socket.io'

import 'assets/css/index.css'
import 'assets/css/shine.css'
import 'assets/css/reactive.css'
import 'assets/font/iconfont.css'


// 谷歌浏览器使用用户浏览器api的前提： localhost  、 https: 、 file:
const SocketIo = new VueSocketIO({
    debug: false,                                // 是否显示连接信息 
    connection: api_domain,   // 连接后端socket的地址(不需要跨域) 
})

createApp(App)
    .use(store) .use(router)
    .use(SocketIo).use(ElementPlus)
    .mount('#app')

