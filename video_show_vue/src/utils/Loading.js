import  {nextTick} from  'vue'
import  {ElLoading} from  'element-plus'

let loadingInstance = null

export const  Loading = {
    open:function(target="body"){
         loadingInstance = ElLoading.service({target})
    },
    close:function(){
        // 以服务的方式调用的 Loading 需要异步关闭
        nextTick().then(() => {
            loadingInstance.close()
        })
    }
}