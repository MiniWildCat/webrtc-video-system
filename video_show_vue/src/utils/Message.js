
import {ElMessage} from  'element-plus'


export  const Message = (type,message,duration=1500)=>{
    // 关闭所有的提示
    ElMessage.closeAll()
    if(type != 'success' && type != 'warning' && type != 'info' && type != 'error' ){
        type = 'info'
    }
    ElMessage({
        type,
        message,
        duration
    })
}