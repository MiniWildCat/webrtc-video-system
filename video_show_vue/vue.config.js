
module.exports ={
    configureWebpack:{
        resolve:{
            alias:{
                'assets':'@/assets',
                'components':'@/components',
                'views':'@/views',
                'config':'@/config',
                'network':'@/network',
                'utils':'@/utils'
            }
        }
    },
    // 注意：不要添加下面一行，否则路由引入会出现异常
    // publicPath:'./'

    // 用于内网穿透使用
    devServer: {
        disableHostCheck: true,
        // historyApiFallback: true,
        // allowedHosts: ["172.18.4.58",'h4419101z1.vicp.fun'],
    },

}